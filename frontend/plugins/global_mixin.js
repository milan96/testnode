import Vue from "vue";
import { mapGetters, mapActions } from "vuex";

Vue.mixin({
  computed: {
    ...mapGetters("auth", {
      isAuth: "isAuth",
      token: "token"
    })
  },
  methods: {
    ...mapActions("auth", {
      logout: "logout"
    }),
    async ajaxGet(url) {
      const resp = await this.$axios.$get(`${url}?token=${this.token}`)
      return resp
    },
    async ajaxPut(url, data) {
      const resp = await this.$axios.$put(`${url}?token=${this.token}`, data)
      return resp
    },
    async ajaxPost(url, data) {
      try {
        const resp = await this.$axios.$post(`${url}?token=${this.token}`, data)
        return Promise.resolve(resp)
      } catch (error) {   
        return Promise.reject(error)
      }

    }
  },
  mounted() { }
});
