const User = require("../models/user")
const { validationResult } = require("express-validator/check")
const bcrypt = require("bcryptjs")
const jwt = require('jsonwebtoken')

exports.login = (req, res, next) => {
    console.log(req.query);
    const username = req.query.username
    const password = req.query.password
    let user;
    User.findOne({ username: username }).then(usr => {
        if (!usr) {
            const error = new Error('User with that username is not exists')
            error.statusCode = 404
            throw error
            // If user is not found, returns 404;
        }
        user = usr
        return bcrypt.compare(password, user.password)
    }).then(isPwd => {
        if (!isPwd) {
            const error = new Error('Password is wrong')
            error.statusCode = 400
            throw error
            // In case of an error in the parameters, returns 400;
        } else {
            const token = jwt.sign({ email: user.email, userId: user._id.toString() },
                'secretsuperbigsecret', {  })
            res.status(200).json({ token: token, id: user._id.toString() })
        }
    }).catch(err => {
        if (!err.statusCode) {
            err.statusCode = 500
        }
        next(err)
    })

}