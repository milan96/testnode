const User = require("../models/user")
const { validationResult } = require("express-validator/check")
const bcrypt = require("bcryptjs")
const jwt = require('jsonwebtoken')
const generator = require('generate-password');

exports.signup = (req, res, next) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        // In case of an error in the parameters, returns 400;
        const error = new Error(`Validation Failed`)
        error.statusCode = errors.array()[0].msg == 'E-mail adress already exists!' || errors.array()[0].msg == 'Username already exists!' ? 409 : 400;
        error.data = errors.array()
        throw error
    }
    const email = req.body.email
    const firstName = req.body.firstName
    const secondName = req.body.secondName
    const password = req.body.password
    const username = req.body.username
    const phoneNumber = req.body.phoneNumber
    const country = req.body.country
    const skype = req.body.skype ? req.body.skype : ''
    let created_user;

    bcrypt.hash(password, 12).then(hp => {
        const user = new User({
            email: email,
            firstName: firstName,
            secondName: secondName,
            username: username,
            password: hp,
            phoneNumber: phoneNumber,
            country: country,
            skype: skype
        })


        created_user = user
        return user.save()
    }).then(result => {
        const token = jwt.sign({ email: email, userId: created_user._id.toString() },
            'secretsuperbigsecret', {})
        res.status(200).json({ message: "Success", userId: result._id, token: token })
        // В случае успеха, возвращает 200 и токен пользователя;
    }).catch(err => {
        if (!err.statusCode) {
            err.statusCode = 500
        }
        next(err)
    })
}

exports.getUser = (req, res, next) => {

    const token = req.query.token

    let decodedToken
    try {
        decodedToken = jwt.verify(token, 'secretsuperbigsecret')
    } catch (error) {
        error.statusCode = 500;
        throw error
    }

    if (!decodedToken) {
        const error = new Error('Not authenticated')
        error.statusCode = 401;
        throw error
    }

    User.findOne({ email: decodedToken.email }).then(usr => {
        let resp = usr.toObject()
        delete resp.password
        res.status(200).json({ user: resp })
    }).catch(err => {
        if (!err.statusCode) {
            err.statusCode = 500
        }
        next(err)
    })
}

exports.editUser = (req, res, next) => {
    console.log(req.body);

    const token = req.query.token

    let decodedToken
    try {
        decodedToken = jwt.verify(token, 'secretsuperbigsecret')
    } catch (error) {
        error.statusCode = 500;
        throw error
    }

    if (!decodedToken) {
        const error = new Error('Not authenticated')
        error.statusCode = 401;
        throw error
    }

    User.findOne({ email: decodedToken.email }).then(usr => {
        for (const key in req.body) {
            if (req.body[key] != usr[key]) {
                usr[key] = req.body[key]
            }

        }
        usr.save()
        res.status(200).json({ user: usr })
    }).catch(err => {
        if (!err.statusCode) {
            err.statusCode = 500
        }
        next(err)
    })
}

exports.changePassword = (req, res, next) => {

    const token = req.query.token

    let decodedToken

    try {
        decodedToken = jwt.verify(token, 'secretsuperbigsecret')
    } catch (error) {
        error.statusCode = 500;
        throw error
    }

    if (!decodedToken) {
        const error = new Error('Not authenticated')
        error.statusCode = 401;
        throw error
    }
    var u
    User.findOne({ email: decodedToken.email }).then(usr => {
        u = usr
        return bcrypt.compare(req.body.oldPassword, usr.password)
    }).then(isPwd => {
        if (!isPwd) {
            const error = new Error('Password is wrong')
            error.statusCode = 400
            throw error
            // In case of an error in the parameters, returns 400;
        } else {
            bcrypt.hash(req.body.password, 12).then(hp => {
                u.password = hp
                u.save()
                res.status(200).json({ message: 'Success', user: u })
            })

        }
    }).catch(err => {
        if (!err.statusCode) {
            err.statusCode = 500
        }
        next(err)
    })
}

exports.resetPassword = (req, res, next) => {

    const token = req.query.token

    let decodedToken

    try {
        decodedToken = jwt.verify(token, 'secretsuperbigsecret')
    } catch (error) {
        error.statusCode = 500;
        throw error
    }

    if (!decodedToken) {
        const error = new Error('Not authenticated')
        error.statusCode = 401;
        throw error
    }
    User.findOne({ email: decodedToken.email }).then(usr => {
        var password = generator.generate({
            length: 10,
            numbers: true
        });

        bcrypt.hash(password, 12).then(hp => {
            usr.password = hp
            usr.save()
            res.status(200).json({ message: 'Success', pwd: password })
        })


    }).catch(err => {
        if (!err.statusCode) {
            err.statusCode = 500
        }
        next(err)
    })
}