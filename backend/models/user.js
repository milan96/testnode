const mongoose = require("mongoose")
const Schema = mongoose.Schema

const userSchema = new Schema({
    username: String,
    firstName: String,
    secondName: String,
    email: String,
    password: String,
    sponsor: {
        type: String,
        default: 'testareg1'
    },
    birthday: Date,
    phoneNumber: Number,
    phoneNumber2: Number,
    skype: String,
    country: String,
    state: String,
    city: String,
    adress: String,
    zipCode: String,
    site: String,
    odnoklassniki: String,
    vk: String,
    fb: String,
    youtube: String,
    autoExtensionBS: Number,
    showMobile: Number,
    showEmail: Number,
    showName: Number,
    deliveryEmail: Number,
    deliverySMS: Number,
    pwdResetCode: String
})

module.exports = mongoose.model("User", userSchema)