const express = require("express")
const router = express.Router()
const authController = require("../controllers/auth")
const { body } = require("express-validator/check")
const User = require("../models/user")

router.get('/', authController.login)

module.exports = router;