const express = require("express")
const router = express.Router()
const UserController = require("../controllers/user")
const { body } = require("express-validator/check")
const User = require("../models/user")
const isAuth = require("../middleware/is-auth")

router.post('/', [body('email').isEmail().withMessage('Please enter valid email').custom((value, { req }) => {
    return User.findOne({ email: value }).then(userDoc => {
        if (userDoc) {
            return Promise.reject("E-mail adress already exists!")
        }
    })
}).normalizeEmail(),
body("username").custom((value, { req }) => {

    if (value[0] == "-" || value[value.length - 1] == "-" || value[0] == "_" || value[value.length - 1] == "_") {
        return Promise.reject("You can not use '-' or '_' on first and last place")
    } else {
        return Promise.resolve()
    }

}).custom((value, { req }) => {

    return User.findOne({ username: value }).then(userDoc => {
        if (userDoc) {
            return Promise.reject("Username already exists!")
        } else {
            return Promise.resolve()
        }
    })
}).custom((value, { req }) => {

    let search = value.match(/[!@#$%^&*(),.?":{}|<>+' ']/g)
    if (search) {
        return Promise.reject("Username contains special caracters or whitespace!")
    } else {
        return Promise.resolve()
    }

}).custom((value, { req }) => {

    let search = value.match(/[A-Z]/g)
    if (search) {
        return Promise.reject("Username contains uppercase!")
    } else {
        return Promise.resolve()
    }

}),
body('firstName').trim().not().isEmpty().isAlphanumeric(),
body('secondName').trim().not().isEmpty().isAlphanumeric(),
body('phoneNumber').custom((value, { req }) => {
    let numeric = value.match(/[^0-9]/g)
    if (numeric) {
        return Promise.reject("This is not a phoneNumber number!")
    } else {
        return Promise.resolve()
    }
}),
body('country').isLength(2),
body('skype').custom((value, { req }) => {
    if (value) {
        let search = value.match(/[!@#$%^&*(),.?":{}|<>+' ']/g)
        if (search) {
            return Promise.reject("Username contains special caracters or whitespace!")
        } else {
            return Promise.resolve()
        }
    } else {
        return Promise.resolve()
    }
})

], UserController.signup)

router.get('/', isAuth, UserController.getUser)

router.put('/', isAuth, UserController.editUser)

router.post('/changePassword', isAuth, UserController.changePassword)

router.post('/resetPassword', isAuth, UserController.resetPassword)

module.exports = router;